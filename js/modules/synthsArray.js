const synthsArray = {
    drum: {
      // DRUM
      kick: {
        obj: new Tone.MembraneSynth({
          "envelope": {
            "attack": 0,
            "decay": 0.1,
            "sustain": 0.3,
            "release": 0.2,
          }
        }),
        note: "C2"
      },
      snare: {
        obj: new Tone.MembraneSynth({
          "envelope": {
            "attack": 0,
            "decay": 0.1,
            "sustain": 0.3,
            "release": 0.002,
          }
        }),
        note: "F2"
      },
      hh: {
        obj: new Tone.MembraneSynth({
          "pitchDecay": 0.5,
          "octaves": 5,
          "envelope": {
            "attack": 0,
            "decay": 0.001,
            "sustain": 0,
            "release": 0.001,
            "attackCurve": "exponential"
          }
        }),
        note: "C5"
      },
    },
    bass: [
      {
        obj: new Tone.DuoSynth(),
        note: "C2"
      },
      {
        obj: new Tone.DuoSynth(),
        note: "D2"
      },
      {
        obj: new Tone.DuoSynth(),
        note: "E2"
      },
      {
        obj: new Tone.DuoSynth(),
        note: "F2"
      },
      {
        obj: new Tone.DuoSynth(),
        note: "G2"
      },
      {
        obj: new Tone.DuoSynth(),
        note: "A2"
      },
      {
        obj: new Tone.DuoSynth(),
        note: "B2"
      },
      {
        obj: new Tone.DuoSynth(),
        note: "C3"
      },
      {
        obj: new Tone.DuoSynth(),
        note: "D3"
      },
    ],
    piano: [
      {
        obj: new Tone.Synth(),
        note: "C3"
      },
      {
        obj: new Tone.Synth(),
        note: "D3"
      },
      {
        obj: new Tone.Synth(),
        note: "E3"
      },
      {
        obj: new Tone.Synth(),
        note: "F3"
      },
      {
        obj: new Tone.Synth(),
        note: "G3"
      },
      {
        obj: new Tone.Synth(),
        note: "A3"
      },
      {
        obj: new Tone.Synth(),
        note: "B3"
      },
      {
        obj: new Tone.Synth(),
        note: "C4"
      },
      {
        obj: new Tone.Synth(),
        note: "D4"
      },
      {
        obj: new Tone.Synth(),
        note: "E4"
      },
      {
        obj: new Tone.Synth(),
        note: "F4"
      },
      {
        obj: new Tone.Synth(),
        note: "G4"
      },
      {
        obj: new Tone.Synth(),
        note: "A4"
      },
      {
        obj: new Tone.Synth(),
        note: "B4"
      },
      {
        obj: new Tone.Synth(),
        note: "C5"
      }
    ]
};
  

export default { synthsArray };