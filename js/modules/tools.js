const container = document.querySelector('.audio-fx')

/**
 * 
 * @param {string} name 
 * @param {number} param1 
 * @param {number} number 
 * @param {sting} titleBox 
 * @returns HTMLInputElements
 */
function drawSlider(name, { min = [0], max = [10], step = [1], value = [0] }, number = 1, titleBox = "") {

  // Return preparation
  const elementsArray = {}

  // randomColor:
  let randHue = Math.random() * 360
  let color = `hsl(${randHue}, 50%, 35%)`

  // slider(s) container
  const div = document.createElement("div")
  div.className = `audio-fx-container seq-${titleBox}`
  div.style.backgroundColor = color

  // titleBox
  if (titleBox !== "") {
    const title = document.createElement("p")
    title.className = "audio-fx-title"
    title.innerHTML = titleBox.toUpperCase()

    // Append title
    div.append(title)
  }


  // Loop to make one or more Input
  for (let i = 0; i < number; i++) {

    // Create parent element
    const slider = document.createElement("div")
    slider.className = "slider-container"

    // clean Name
    const cleanName = name[i].replace(/\//g, "_")

    // Init ctrl values
    let stepV, valueV, minV, maxV;

    // Check values if exist
    step[i] ? stepV = step[i] : stepV = step[0];
    value[i] ? valueV = value[i] : valueV = value[0];
    min[i] ? minV = min[i] : minV = min[0];
    max[i] ? maxV = max[i] : maxV = max[0];

    // INNER HTML
    slider.innerHTML =
      `<div class="slider-value" id="${cleanName}_val">0</div>
            <input orient="vertical" type="range" id="${cleanName}_range" step="${stepV}" value="${valueV}" min="${minV}" max="${maxV}">
            <div class="slider-info">${name[i].toUpperCase()}</div>`

    // Append
    div.append(slider)
    container.append(div)

    const element = document.getElementById(`${cleanName}_range`)
    const displayVal = document.getElementById(`${cleanName}_val`)

    // Display good values.
    let inHtmlValue = element.value
    if (element.value % 1 !== 0) {
      inHtmlValue = Number(element.value).toFixed(2);
    }
    displayVal.textContent = inHtmlValue
    
    // Push Elements into array
    elementsArray[`${cleanName}`] = element
    if (i>=1) {
      elementsArray['value'+i] = displayVal      
    } else {
      elementsArray['value'] = displayVal
    }
  }

  // Return elements Array
  return elementsArray;
}


/**
 * Initialize Array of the Synths, and concat drums, bass and piano.
 * @param {array} synthsArray 
 * @returns the synth array.
 */
function concatSynthArray(synthsArray) {

  // Get the array
  synthsArray = synthsArray.synthsArray

  // Random Oscillator TYPE  - && -  VOLUME CONTROL (PIANO-KEYS);
  const type = ["triangle", "square", "sawtooth", "sine"];
  synthsArray.piano.forEach((synth) => {
    synth.obj.volume.value = -6;
    synth.obj.oscillator.type = (type[(Math.floor(Math.random() * type.length))]);
    let synthType = synth.obj.oscillator.type;
    if (synthType == 'square' || synthType == 'sawtooth' || synthType == 'sine') {
      synth.obj.volume.value = -15;
    }
  });

  // BASSE VOLUME ;
  synthsArray.bass.forEach((synth) => {
    synth.obj.volume.value = -20;
  });

  // DRUM SYNTH NOTE CONTROL ;
  synthsArray.drum.kick.obj.octaves = 2;
  synthsArray.drum.kick.obj.volume.value = 0;
  synthsArray.drum.snare.obj.octaves = 10;
  synthsArray.drum.snare.obj.volume.value = -10;
  synthsArray.drum.hh.obj.octaves = 10;
  synthsArray.drum.hh.obj.volume.value = -10;



  return [
    synthsArray.drum.hh,
    synthsArray.drum.snare,
    synthsArray.drum.kick,
  ].concat(synthsArray.bass).concat(synthsArray.piano)
}


export { drawSlider, concatSynthArray };
