import * as Tools from './modules/tools.js'
import synthsArray from './modules/synthsArray.js'


document.querySelector('._start_app')?.addEventListener('click', async () => {
    await Tone.start()
    document.querySelector('._start_app').classList.toggle('hidden', true)
	console.log('audio is ready')
})


// Create and Draw Sliders elements.
const sliderVolume = Tools.drawSlider(["vol"], { step: [.001], min: [0], max: [.5], value: [0.1] }, 1, "volume")
const sliderBpm = Tools.drawSlider(["bpm"], { step: [1], min: [60], max: [200], value: [120] }, 1, "tempo")
const sliderReverb = Tools.drawSlider(["reverb", "room", "damp"], {
    step: [.001, .001, 1],
    min: [0, 0, 1],
    max: [1, .98, 8000],
    value: [0, 0, 3000]
}, 3, "reverb")
const sliderDelay = Tools.drawSlider(["delay"], { step: [.01], min: [0], max: [1], value: [0] }, 1, "ping_pong")
const sliderPitch = Tools.drawSlider(["pitch"], { step: [.01], min: [0], max: [1], value: [0.5] }, 1, "picth")
const sliderEq = Tools.drawSlider(["low", "med", "high"], {
    min: [-20, -20, -20],
    max: [20, 20, 20],
    value: [0, 0, 0]
}, 3, "equaliser")
const sliderMidSide = Tools.drawSlider(["m/s"], { step: [.01], min: [0], max: [1], value: [.5] }, 1, "mid/side")
const sliderCompression = Tools.drawSlider(["comp"], { min: [-50], max: [0], value: [0] }, 1, "compression")
// Button Preview
const previewBtn = document.getElementById('preview_btn')


// NEXUS UI
const sequencer = new Nexus.Sequencer('#sequencer', {
    'size': [800, 600],
    'mode': 'toggle',
    'rows': 27,
    'columns': 16,
})
// play / stop
const startButton = document.getElementById('_play')
startButton.onclick = () => playStop();
window.addEventListener('keydown', (e) => {
    if (e.code === "Space") {
        playStop();
    }
})
function playStop() {
    if (startButton.textContent === "PLAY") {
        Tone.start()
        sequencer.stepper.value = -1
        sequencer.start()
        startButton.textContent = "STOP"
    } else {
        sequencer.stop();
        startButton.textContent = "PLAY"
    }
}


// TONE
const synths = Tools.concatSynthArray(synthsArray);


// GAIN MASTER
const gain = new Tone.Gain(0.1);
const reverb = new Tone.Freeverb();  // Roomsize (0 et 0.98), Dampening (1000, 8000...)
reverb.wet.value = 0;  // Dry & Wet Mix. (0 = 100% dry) &  (1 = 100% effect)
const delay = new Tone.PingPongDelay(0.25, 0.2);  //  ( [ delayTime] , [ feedback] ) 
delay.wet.value = 0
const pitch = new Tone.PitchShift(1);
const wide_stereo = new Tone.StereoWidener()
const equalizer = new Tone.EQ3({
    "lowFrequency": 400,
    "highFrequency": 2500
})
const compressor = new Tone.Compressor({
    "ratio": 12,
    "threshold": -0,
    "release": 0.25,
    "attack": 0.003,
    "knee": 30
})


// CONNECTING
reverb.connect(delay);
delay.connect(pitch);
pitch.connect(wide_stereo);
wide_stereo.connect(equalizer)
equalizer.connect(compressor)
compressor.connect(gain)
gain.toDestination()

synths.forEach(synth => synth.obj.connect(reverb))


Tone.Transport.bpm.value = 200


// SEQUENCER
let soundPreview = 1
previewBtn.onclick = () => {
    if (previewBtn.textContent === "ON") {
        soundPreview = 0
        previewBtn.textContent = "OFF"
    } else {
        soundPreview = 1
        previewBtn.textContent = "ON"
    }
    previewBtn.classList.toggle("active")
}
// Sound on click 
sequencer.on('change', function (v) {
    if (soundPreview >= 1) {
        let state = v.state;
        let synth = synths[v.row]
        if (state) { synth.obj.triggerAttackRelease(synth.note, '8n') }
    }
});

// Sound on play
sequencer.on('step', function (e) {
    for (let i = 0; i < e.length; i++) {
        const synth = synths[e.length - (i + 1)];
        if (e[i] === 1) {
            synth.obj.triggerAttackRelease(synth.note, '8n')
        }
    }
})


// LISTENERS
sliderVolume.vol.oninput = () => { 
    gain.gain.value = sliderVolume.vol.value
    sliderVolume.value.textContent = displayValue(sliderVolume.vol.value)
}
sliderBpm.bpm.oninput = () => {
    // Tone.Transport.bpm.value = sliderBpm.bpm.value
    // sliderBpm.value = sliderBpm.bpm.value
}
sliderReverb.reverb.oninput = () => {
    reverb.wet.value = sliderReverb.reverb.value
    sliderReverb.value.textContent = displayValue(sliderReverb.reverb.value)
}
sliderReverb.room.oninput = () => {
    reverb.roomSize.value = sliderReverb.room.value
    sliderReverb.value1.textContent = displayValue(sliderReverb.room.value)
}
sliderReverb.damp.oninput = () => {
    reverb.dampening = sliderReverb.damp.value
    sliderReverb.value2.textContent = displayValue(sliderReverb.damp.value)
}
sliderDelay.delay.oninput = () => {
    delay.wet.value = sliderDelay.delay.value
    sliderDelay.value.textContent = displayValue(sliderDelay.delay.value)
}
sliderPitch.pitch.oninput = () => {
    pitch.wet.value = sliderPitch.pitch.value
    sliderPitch.value.textContent = displayValue(sliderPitch.pitch.value)
}
sliderEq.low.oninput = () => {
    equalizer.low.value = sliderEq.low.value
    sliderEq.value.textContent = displayValue(sliderEq.low.value)
}
sliderEq.med.oninput = () => {
    equalizer.mid.value = sliderEq.med.value
    sliderEq.value1.textContent = displayValue(sliderEq.med.value)
}
sliderEq.high.oninput = () => {
    equalizer.high.value = sliderEq.high.value
    sliderEq.value2.textContent = displayValue(sliderEq.high.value)
}
sliderMidSide.m_s.oninput = () => {
    wide_stereo.width.value = sliderMidSide.m_s.value
    sliderMidSide.value.textContent = displayValue(sliderMidSide.m_s.value)
}
sliderCompression.comp.oninput = (e) => {
    compressor.threshold.value = sliderCompression.comp.value
    sliderCompression.value.textContent = displayValue(sliderCompression.comp.value)
}

function displayValue(value) {
    let val = value
    if (value % 1 !== 0) {
        val = Number(value).toFixed(2);
    }
    return val;
}